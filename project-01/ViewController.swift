//
//  ViewController.swift
//  project-01
//
//  Created by Andrei on 4/13/18.
//  Copyright © 2018 Andrei. All rights reserved.
//

import UIKit

class ViewController: UITableViewController {
    
    var cryptoImages = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        loadImages()
        cryptoImages.sort()
    }
    
    private func setup() {
        navigationController?.navigationBar.topItem?.title = "Crypto Images"
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "CryptoImageCell")
    }
    
    private func loadImages() {
        let fm = FileManager.default
        let path = Bundle.main.resourcePath!
        let items = try! fm.contentsOfDirectory(atPath: path)
        
        for item in items {
            if item.hasPrefix("crypto") {
                // this is a picture to load!
                cryptoImages.append(item)
            }
        }
    }
}

extension ViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cryptoImages.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CryptoImageCell", for: indexPath)
        cell.textLabel?.text = cryptoImages[indexPath.row]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailVC = DetailViewController()
        detailVC.selectedImage = cryptoImages[indexPath.row]
        let nc = UINavigationController(rootViewController: detailVC)
        self.present(nc, animated: true, completion: nil)
    }
}

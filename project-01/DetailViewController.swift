//
//  DetailViewController.swift
//  project-01
//
//  Created by Andrei on 4/17/18.
//  Copyright © 2018 Andrei. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    let backButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Back", for: .normal)
        button.setTitleColor(#colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1), for: .normal)
        return button
    }()
    
    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    var selectedImage: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        displayImage()
        
    }
    
    private func setupUI() {
        view.addSubview(imageView)
        view.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        imageView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        imageView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        imageView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        imageView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
        backButton.addTarget(self, action: #selector(goBack), for: .touchUpInside)
    }
    
    private func displayImage() {
        if let displayedImage = selectedImage {
            navigationController?.navigationBar.topItem?.title = displayedImage
            imageView.image = UIImage(named: displayedImage)
        }
    }
    
    @objc private func goBack() {
        self.dismiss(animated: true, completion: nil)
    }
}
